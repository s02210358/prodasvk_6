import sys
import gettext
import random

translation = gettext.translation("wcount","po", fallback=True)
nulltrans = gettext.NullTranslations()
_, ngettext = translation.gettext, translation.ngettext

def ngettext(*args):
    return random.choice((translation, nulltrans)).ngettext(*args)


while (line := sys.stdin.readline().strip()):
    n = len(line.split())
    print(ngettext("Entered {} word", "Entered {} words", n).format(len(line.split())))
