��          �               �   )   �   4        L  
   e     p     �  H   �  d   �     I     [     c  "   s  �  �  G   v  �   �  )   q     �  (   �  '   �  E    E  G  2   �     �     �  �   �   Added monster {} to ({}, {}) saying: "{}" Attacked {}, damage {} hp Attacked {}, damage {} hps Locale {} does not exist No {} here Replaced the old monster Set up locale: {} User {} added monster {} with {} hp User {} added monster {} with {} hps User {} attacked monster {} with {}, damage {} hp User {} attacked monster {} with {}, damage {} hps User {} connected {} died {} disconnected {} now has {} hp {} now has {} hps Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2024-04-20 11:50+0300
PO-Revision-Date: 2024-04-20 12:02+0300
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: ru_RU
Language-Team: ru_RU <LL@li.org>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.14.0
 Добавлен монстр {} на ({}, {}) говорящий: "{}" Атакован {}, урон {} очко здоровья Атакован {}, урон {} очка здоровья Атакован {}, урон {} очков здоровья Локаль {} не существует {} здесь нет Заменен старый монстр Установлена локаль: {} Пользователь {} добавил монстра {}, имеющего {} очко здоровья Пользователь {} добавил монстра {}, имеющего {} очка здоровья Пользователь {} добавил монстра {}, имеющего {} очков здоровья Пользователь {} атаковал монстра {} с {}, урон {} очко здоровья Пользователь {} атаковал монстра {} с {}, урон {} очка здоровья Пользователь {} атаковал монстра {} с {}, урон {} очков здоровья Пользователь {} подключился {} умер {} отключился {} теперь имеет {} очко здоровья {} теперь имеет {} очка здоровья {} теперь имеет {} очков здоровья 