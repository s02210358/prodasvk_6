import sys
import cowsay
from custom_monsters import custom_monsters
import io
import shlex

class Field:
	def __init__(self):
		self.char_pos = {}
	def get_character(self, x: int, y:int):
		return self.char_pos[(x, y)]
	def check_position(self, x: int, y: int):
		return (x, y) in self.char_pos.keys()
	def set_character(self, x: int, y: int, other):
		self.char_pos[(x, y)] = other
	def encounter(self, x, y):
		monster = self.get_character(x, y)
		name = monster.get_name()

		if name in cowsay.list_cows():
			print(cowsay.cowsay(monster.get_phrase(), cow=name))
		elif name in custom_monsters:
			custom_cow = cowsay.read_dot_cow(io.StringIO(custom_monsters[name]))
			print(cowsay.cowsay(monster.get_phrase(), cowfile=custom_cow))

class Character:
	x = 0
	y = 0

	def __init__(self, field: Field):
		self.field = field

	def get_position(self):
		return (self.x, self.y)
	def set_position(self, x: int, y: int):
		self.x, self.y = x, y

class Monster(Character):
	def __init__(self, x: int, y: int, name: str, phrase: str, hp: int, field: Field):
		super().__init__(field)
		self.x = x
		self.y = y
		self.name = name
		self.phrase = phrase
		self.hp = hp
		self.field.set_character(self.x, self.y, self)
	def get_phrase(self):
		return self.phrase
	def get_name(self):
		return self.name
	def get_hp(self):
		return self.hp

print("<<< Welcome to Python-MUD 0.1 >>>")

desk = Field()
hero = Character(desk)

while (line :=  sys.stdin.readline()):
	match shlex.split(line):
		case [('up'|'down'|'left'|'right') as move]:
			x, y = hero.get_position()

			match move:
				case 'up':
					y = (y - 1)%10
				case 'down':
					y = (y + 1)%10
				case 'left':
					x = (x - 1)%10
				case 'right':
					x = (x + 1)%10

			hero.set_position(x, y)
			print(f'Moved to ({x}, {y})')
			
			if desk.check_position(x, y):
				desk.encounter(x, y)
		case ['addmon', name, *opt]:
			if len(opt) != 7:
				print('Invalid arguments')
				continue

			if (name not in cowsay.list_cows()) and \
					(name not in custom_monsters):
				print('Cannot add unknown monster')
				continue

			while opt:
				cnt = 0

				match opt:
					case ['hello', param, *tmp]:
						phrase = param
						cnt = 2
					case ['hp', param, *tmp]:
						try:
							hp = int(param)
						except Exception:
							break

						if (hp <= 0):
							break

						cnt = 2
					case ['coords', p1, p2, *tmp]:
						try:
							x, y = int(p1), int(p2)
						except Exception:
							break

						if (x >= 10 or x < 0) or (y >= 10 or y < 0):
							break

						cnt = 3
					case _:
						break

				opt = opt[cnt:]
			else:
				flag = desk.check_position(x, y)

				Monster(x, y, name, phrase, hp, desk)
				print(f'Added monster {name} to ({x}, {y}) saying {phrase}')

				if flag: print('Replaced the old monster')
				continue

			print('Invalid arguments')
		case _:
			print('Invalid command')
