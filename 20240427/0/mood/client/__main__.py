"""Start client."""
import socket
import threading
import sys
from ..client import client as cl


name = sys.argv[1]
host = "localhost" if len(sys.argv) < 3 else sys.argv[2]
port = 1337 if len(sys.argv) < 4 else int(sys.argv[3])

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((host, port))
    s.sendall(f'login {name}\n'.encode())
    cmd = cl.MUD(s)
    f = int(s.recv(2).rstrip().decode())
    if f:
        print(f'User {name} registered', end='\n\n')
        tread = threading.Thread(target=cl.read_chat, args=[s, cmd]).start()
        cmd.cmdloop()
    else:
        print(f"User with name {name} already exists")
