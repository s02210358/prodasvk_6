"""Start server and roaming_monster function."""
from ..server import server as srv
import sys

port = 1337 if len(sys.argv) < 2 else int(sys.argv[1])


async def main():
    """Start roaming_monster and asyncio server."""
    srv.task = srv.asyncio.create_task(srv.roaming_monster())
    await srv.asyncio.sleep(0)
    server = await srv.asyncio.start_server(srv.mud, '0.0.0.0', port)
    async with server:
        await server.serve_forever()

srv.asyncio.run(main())
