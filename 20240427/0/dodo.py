def task_html():
    return {
            'actions': ["sphinx-build -M html 'docs/source' 'docs/build'"],
            'file_dep': ["docs/source/index.rst", "docs/source/API.rst"]
    }

def task_erase():
    return {
            'actions': ["git clean -xdf"]
    }
