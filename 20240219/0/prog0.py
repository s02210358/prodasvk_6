import sys

with open(sys.argv[1], 'rb') as f:
    content = f.read()

import zlib

print(zlib.decompress(content))
