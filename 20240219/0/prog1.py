import sys
import glob
import zlib

PATH = '../../.git/objects/'

for i in glob.iglob(PATH + "??/*"):
    with open(i, 'rb') as f:
        content = f.read()
        content = zlib.decompress(content)
        
        if b"commit" in content:
            print(content.partition(b'\x00')[2].decode(), end='\n---------\n')
        elif b'tree' in content:
            content = content.partition(b'\x00')[2]
            while content:
                name, _, content = content.partition(b'\x00')
                ID, content = content[:20].hex(), content[20:]
                print(name, ID)

            print("\n-------\n")
        else:
            print(content, end='\n-------\n')
