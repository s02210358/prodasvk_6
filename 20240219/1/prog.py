import sys
import zlib
import glob

if len(sys.argv) > 1:
	PATH = sys.argv[1] + '/.git/refs/heads/'

if len(sys.argv) == 2: 
#реализация вывода списка веток
	for i in glob.iglob(PATH + '*'):
		print(i[len(PATH):])
elif len(sys.argv) == 3: 
#реализации вывода дерева для последовательности коммитов
	with open(PATH + sys.argv[2], 'r') as f:
		commit = f.read()[0:-1]

	while commit:
		cur_commit = commit
		lib = commit[0:2]
		commit = commit[2:]

		PATH = sys.argv[1] + '/.git/objects/' + lib + '/' + commit 

		with open(PATH, 'rb') as f:
			commit_data = f.read()

		commit_data = zlib.decompress(commit_data)
		commit_data = commit_data.partition(b'\x00')[2].decode()


		tmp = commit_data.partition('tree ')[2]
		tmp, _, tail = tmp.partition('\n')
		lib = tmp[0:2]
		tree = tmp[2:]
		if 'parent' in tail:
			tail = tail.partition('parent ')[2]
			commit = tail.partition('\n')[0]
		else:
			commit = 0

		PATH = sys.argv[1] + '/.git/objects/' + lib + '/' + tree

		with open(PATH, 'rb') as f:
			tree = f.read()

		tree = (zlib.decompress(tree)).partition(b'\x00')[2]

		print("TREE for commit", cur_commit)

		while tree:
			name, _, tree = tree.partition(b'\x00')
			ID, tree = tree[:20].hex(), tree[20:]
			teg, _, name = name.decode().partition(' ')
			lib, obj = ID[0:2], ID[2:]
			PATH = sys.argv[1] + '/.git/objects/' + lib + '/' + obj

			with open(PATH, 'rb') as f:
				data = f.read()

			teg = (zlib.decompress(data)).partition(b' ')[0].decode()
			print(teg, ID, '  ', name)

	




