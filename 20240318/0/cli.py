import sys
import socket
import shlex
import cmd

class client(cmd.Cmd):
    prompt = '>> '

    def __init__(self, socet):
        super().__init__()

        self.s = socet

    def do_info(self, args):
        self.s.sendall(("info" + args).encode())
        print(self.s.recv(1024).rstrip().decode())
    def do_print(self, args):
        self.s.sendall(("print" + args).encode())
        print(self.s.recv(1024).rstrip().decode())
    def complete_info(self, text, line, begidx, endidx):
        line = shlex.split(line)
        return [i for i in ['port', 'host'] if (i.startswith(text) and (line[-1] == 'info' or line[-2] == 'info'))]

host = "localhost" if len(sys.argv) < 2 else sys.argv[1]
port = 1337 if len(sys.argv) < 3 else int(sys.argv[2])
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socet:
    socet.connect((host, port))
    client(socet).cmdloop()


