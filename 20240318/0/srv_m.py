import socket
import sys
import multiprocessing
import shlex

def serve(conn, addr):
    with conn:
      print('Connected by', addr)
      while data := conn.recv(1024):
          match shlex.split(data.decode()):
              case["print", message]:
                  conn.sendall(message.encode())
              case["info", "host"]:
                  conn.sendall(f"{addr[0]}".encode())
              case["info", "port"]:
                  conn.sendall(f"{addr[1]}".encode())
host = "localhost" if len(sys.argv) < 2 else sys.argv[1]
port = 1337 if len(sys.argv) < 3 else int(sys.argv[2])
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((host, port))
    s.listen()
    while True:
        conn, addr = s.accept()
        multiprocessing.Process(target=serve, args=(conn, addr)).start()
    s.close()
