import sys
import socket
import cowsay
from custom_monsters import custom_monsters
import shlex
import cmd
import readline
import io

armory = ["sword", "spear", "axe"]

def move_hero(direction: str, cmd_socket):
    x, y = 0, 0
    match direction:
        case "up":
            x, y = 0, -1
        case "down":
            x, y = 0, 1
        case "left":
            x, y = -1, 0
        case "right":
            x, y = 1, 0

    cmd_socket.sendall(f"move {x} {y}".encode())
    msg = cmd_socket.recv(1024).rstrip().decode()
    print("Moved to (" + msg + ")")

    cmd_socket.sendall("yes".encode())

    msg = cmd_socket.recv(1024).rstrip().decode()

    match shlex.split(msg):
        case ["name", name, "phrase", phrase]:
            if name in cowsay.list_cows():
                print(cowsay.cowsay(phrase, cow=name))
            elif name in custom_monsters:
                custom_cow = cowsay.read_dot_cow(io.StringIO(custom_monsters[name]))
                print(cowsay.cowsay(phrase, cowfile=custom_cow))
        case ["empty"]:
            pass

def addmon(options: str, cmd_socket):
    opt = shlex.split(options)
    name = opt[0]
    opt = opt[1:]

    if len(opt) != 7:
        print('Invalid arguments')
        return

    if (name not in cowsay.list_cows()) and \
            (name not in custom_monsters):
        print('Cannot add unknown monster')
        return

    while opt:
        cnt = 0

        match opt:
            case ['hello', param, *tmp]:
                phrase = param
                cnt = 2
            case ['hp', param, *tmp]:
                try:
                    hp = int(param)
                except Exception:
                    break

                if (hp <= 0):
                    break

                cnt = 2
            case ['coords', p1, p2, *tmp]:
                try:
                    x, y = int(p1), int(p2)
                except Exception:
                    break

                if (x >= 10 or x < 0) or (y >= 10 or y < 0):
                    break

                cnt = 3
            case _:
                break

        opt = opt[cnt:]
    else:
        cmd_socket.sendall(f"addmon {name} phrase '{phrase}' hp {hp} coords {x} {y}".encode())
        flag = cmd_socket.recv(1024).rstrip().decode()
        print(f'Added monster {name} to ({x}, {y}) saying: "{phrase}"')

        if int(flag): print('Replaced the old monster')
        return

    print('Invalid arguments')

def attack(options: str, cmd_socket):
    if not options:
        print("Invalid arguments")
        return
        
    args = shlex.split(options)
    name = args[0]
    args = args[1:]
    weapon = "sword"

    if args:
        if args[0] == "with":
            if args[1] not in armory:
                print("Unknown weapon")
                return
            else:
                weapon = args[1]
        else:
            print("Invalid arguments")
            return

    cmd_socket.sendall(f'attack {name} {weapon}'.encode())

    msg = cmd_socket.recv(1024).rstrip().decode()

    match shlex.split(msg):
        case ["damage", damage, "hp", hp]:
            print(f"Attacked {name}, damage {damage} hp")

            if int(hp) == 0:
                print(f'{name} died')
            else:
                print(f'{name} now has {hp} hp')
        case ["no"]:
            print(f'No {name} here')
            

class MUD(cmd.Cmd):
    prompt = '~~> '
    intro = "<<< Welcome to Python-MUD 0.1 >>>\nType help or ? to list commands.\n"

    def __init__(self, s):
        super().__init__()

        self.cmd_socket = s

    #cmd settings
    def do_EOF(self, args):
        '''Ctr+D to quit game'''
        print('\n')
        return True 

    # move hero
    def do_up(self, args):
        '''Moves the hero up one square'''
        move_hero("up", self.cmd_socket)
    def do_down(self, args):
        '''Moves the hero down one square'''
        move_hero("down", self.cmd_socket)
    def do_left(self, args):
        '''Moves the hero left one square'''
        move_hero("left", self.cmd_socket)
    def do_right(self, args):
        '''Moves the hero right one square'''
        move_hero("right", self.cmd_socket)

    # attack monsters
    def do_attack(self, args):
        attack(args, self.cmd_socket)
    def help_attack(self):
        print("attack <name_str> {with <weapon_name>} - Hero attacks monster with name == <name_str> on current square")
        print("\n{with <weapon_name>} - choose weapon (default 'sword': damage 10 hp)")
    def complete_attack(self, text, line, begidx, endidx):
        line = shlex.split(line)
        res = cowsay.list_cows() + list(custom_monsters.keys())

        if line[-1] == "with" or line[-2] == "with":
            return [i for i in armory if i.startswith(text)]
        elif line[-1] == "attack" or line[-2] == "attack":
            return [i for i in res if i.startswith(text)]

    # add new monster on field
    def do_addmon(self, args):
        addmon(args, self.cmd_socket)
    def help_addmon(self):
        print("addmon <name> [hello <hello_string>] [hp <value>] [coords <x> <y>]")
        print("\n<name> - name of monster")
        print("[hello <hello_string>] - phrase that monster say on meeting with hero")
        print("[hp <value>] - monster hp (must be integer and above zero)")
        print("[coords <x> <y>] - coords square on field, where monster will stand (if there is already a monster on the square, then replaces it)")
        print("\t<x> <y> must be integer in [0, 9]")


host = "localhost" if len(sys.argv) < 2 else sys.argv[1]
port = 1337 if len(sys.argv) < 3 else int(sys.argv[2])

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((host, port))
    MUD(s).cmdloop()
