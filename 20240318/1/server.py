import socket
import sys
import shlex

class Field:
    def __init__(self):
        self.char_pos = {}
    def get_character(self, x: int, y:int):
        return self.char_pos[(x, y)]
    def check_position(self, x: int, y: int):
        return (x, y) in self.char_pos.keys()
    def set_character(self, x: int, y: int, other):
        self.char_pos[(x, y)] = other
    def delete_character(self, x: int, y: int):
        del self.char_pos[(x, y)]
    def encounter(self, x, y):
        monster = self.get_character(x, y)
        return (monster.get_name(), monster.get_phrase())


class Character:
    x = 0
    y = 0

    def __init__(self, field: Field):
        self.field = field

    def get_position(self):
        return (self.x, self.y)
    def set_position(self, x: int, y: int):
        self.x, self.y = x, y

class Hero(Character):
    def __init__(self, field: Field):
        super().__init__(field)
        self.x = 0
        self.y = 0
        self.weapon = "sword"
        self.armory = {"sword": 10,
                        "spear": 15,
                        "axe": 20}
    def choose_weapon(self, name):
        if name in self.armory.keys():
            self.weapon = name
            return True
        return False
    def get_damage(self):
        return self.armory[self.weapon]
    def get_armory(self):
        return self.armory.keys()


class Monster(Character):
    def __init__(self, x: int, y: int, name: str, phrase: str, hp: int, field: Field):
        super().__init__(field)
        self.x = x
        self.y = y
        self.name = name
        self.phrase = phrase
        self.hp = hp
        self.field.set_character(self.x, self.y, self)
    def get_phrase(self):
        return self.phrase
    def get_name(self):
        return self.name
    def set_hp(self, new_hp: int):
        self.hp = new_hp
    def get_hp(self):
        return self.hp

def serve(conn, addr):
    with conn:
        print("Connected by", addr)
        while data := conn.recv(1024).rstrip():
            match shlex.split(data.decode()):
                case ["move", a, b]:
                    x, y = hero.get_position()
                    x = (x + int(a))%10
                    y = (y + int(b))%10
                    hero.set_position(x, y)
                    
                    conn.sendall(f"{x}, {y}".encode())

                    msg = conn.recv(1024).rstrip().decode()

                    if msg == "yes":
                        if desk.check_position(x, y):
                            name, phrase = desk.encounter(x, y)
                            conn.sendall(f"name {name} phrase '{phrase}'".encode())
                        else:
                            conn.sendall("empty".encode())
                case ["addmon", name, "phrase", phrase, "hp", hp, "coords", x, y]:
                    x, y = int(x), int(y)
                    hp = int(hp)

                    print("In addmon")
                    
                    flag = desk.check_position(x, y)
                    Monster(x, y, name, phrase, hp, desk)

                    conn.sendall(f"{int(flag)}".encode())
                case ["attack", name, weapon]:
                    x, y = hero.get_position()
                    
                    if desk.check_position(x, y) and (monster := desk.get_character(x, y)).get_name() == name:
                        hero.choose_weapon(weapon)
                        damage = hero.get_damage() if monster.get_hp() >= hero.get_damage() else monster.get_hp()
                        monster.set_hp(monster.get_hp() - damage)
                        conn.sendall(f'damage {damage} hp {monster.get_hp()}'.encode())

                        if monster.get_hp() == 0:
                            desk.delete_character(x, y)
                    else:
                        conn.sendall("no".encode())
                case _:
                    print(shlex.split(data.decode()))

host = "localhost" if len(sys.argv) < 2 else sys.argv[1]
port = 1337 if len(sys.argv) < 3 else int(sys.argv[2])

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((host, port))
    s.listen()
    conn, addr = s.accept()
    desk = Field()
    hero = Hero(desk)
    serve(conn, addr)
