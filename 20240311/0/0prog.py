import cmd
import readline

class Echoer(cmd.Cmd):
    """Dumb echo command REPL"""
    prompt = ":->"
    words = 'one', 'two', 'three', 'four'

    def do_echo(self, arg):
        print(arg)
    def do_EOF(self, arg):
        return True
    def complete_echo(self, text, line, begidx, endidx):
        return [i for i in self.words if i.startswith(text)]
if __name__=='__main__':
    Echoer().cmdloop()
