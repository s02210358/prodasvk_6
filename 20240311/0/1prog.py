import cmd
import shlex
import calendar

class Calendar(cmd.Cmd):
    def do_prmonth(self, arg):
        arg = shlex.split(arg)

        match arg:
            case [year, month, *tmp]:
                try:
                    year = int(year)
                    month = self.Month[month]
                except Exception:
                    print("Invalid")


        calendar.TextCalendar().prmonth(year, month)

    def do_pryear(self, arg):
        arg = shlex.split(arg)

        match arg:
            case [year, *tmp]:
                try:
                    year = int(year)
                except Exception:
                    print("Invalid")

        calendar.TextCalendar().pryear(year)

    def complete_prmonth(self, text, line, begidx, endidx):
        line = shlex.split(line)
        if len(line) >= 2:
            return [i.name for i in calendar.Month if i.name.startswith(text)]

if __name__=='__main__':
    Calendar().cmdloop()
