import sys
import cowsay
from custom_monsters import custom_monsters
import io
import shlex
import cmd
import readline

class Field:
    def __init__(self):
        self.char_pos = {}
    def get_character(self, x: int, y:int):
        return self.char_pos[(x, y)]
    def check_position(self, x: int, y: int):
        return (x, y) in self.char_pos.keys()
    def set_character(self, x: int, y: int, other):
        self.char_pos[(x, y)] = other
    def delete_character(self, x: int, y: int):
        del self.char_pos[(x, y)]
    def encounter(self, x, y):
        monster = self.get_character(x, y)
        name = monster.get_name()

        if name in cowsay.list_cows():
            print(cowsay.cowsay(monster.get_phrase(), cow=name))
        elif name in custom_monsters:
            custom_cow = cowsay.read_dot_cow(io.StringIO(custom_monsters[name]))
            print(cowsay.cowsay(monster.get_phrase(), cowfile=custom_cow))

class Character:
    x = 0
    y = 0

    def __init__(self, field: Field):
        self.field = field

    def get_position(self):
        return (self.x, self.y)
    def set_position(self, x: int, y: int):
        self.x, self.y = x, y

class Hero(Character):
    def __init__(self, field: Field):
        super().__init__(field)
        self.x = 0
        self.y = 0
        self.weapon = "sword"
        self.armory = {"sword": 10,
                        "spear": 15,
                        "axe": 20}
    def choose_weapon(self, name):
        if name in self.armory.keys():
            self.weapon = name
            return True
        return False
    def get_damage(self):
        return self.armory[self.weapon]
    def get_armory(self):
        return self.armory.keys()


class Monster(Character):
    def __init__(self, x: int, y: int, name: str, phrase: str, hp: int, field: Field):
        super().__init__(field)
        self.x = x
        self.y = y
        self.name = name
        self.phrase = phrase
        self.hp = hp
        self.field.set_character(self.x, self.y, self)
    def get_phrase(self):
        return self.phrase
    def get_name(self):
        return self.name
    def set_hp(self, new_hp: int):
        self.hp = new_hp
    def get_hp(self):
        return self.hp

def move_hero(direction: str):
    x, y = hero.get_position()

    match direction:
        case 'up':
            y = (y - 1)%10
        case 'down':
            y = (y + 1)%10
        case 'left':
            x = (x - 1)%10
        case 'right':
            x = (x + 1)%10

    hero.set_position(x, y)
    print(f'Moved to ({x}, {y})')
      
    if desk.check_position(x, y):
        desk.encounter(x, y)

def addmon(opt: str):
    opt = shlex.split(opt)
    name = opt[0]
    opt = opt[1:]

    if len(opt) != 7:
        print('Invalid arguments')
        return

    if (name not in cowsay.list_cows()) and \
            (name not in custom_monsters):
        print('Cannot add unknown monster')
        return

    while opt:
        cnt = 0

        match opt:
            case ['hello', param, *tmp]:
                phrase = param
                cnt = 2
            case ['hp', param, *tmp]:
                try:
                    hp = int(param)
                except Exception:
                    break

                if (hp <= 0):
                    break

                cnt = 2
            case ['coords', p1, p2, *tmp]:
                try:
                    x, y = int(p1), int(p2)
                except Exception:
                    break

                if (x >= 10 or x < 0) or (y >= 10 or y < 0):
                    break

                cnt = 3
            case _:
                break

        opt = opt[cnt:]
    else:
        flag = desk.check_position(x, y)

        Monster(x, y, name, phrase, hp, desk)
        print(f'Added monster {name} to ({x}, {y}) saying {phrase}')

        if flag: print('Replaced the old monster')
        return

    print('Invalid arguments')

def attack(args: str):
    x, y = hero.get_position()

    if not args:
        print("Invalid arguments")
        return
        
    args = shlex.split(args)
    name = args[0]
    args = args[1:]

    if desk.check_position(x, y) and (monster := desk.get_character(x, y)).get_name() == name:
        if args:
            match args:
                case ['with', weapon]:
                    if not hero.choose_weapon(weapon):
                        print("Unknown weapon")
                        return
                case _:
                    print("Invalid arguments")
                    return

        damage = hero.get_damage() if monster.get_hp() >= hero.get_damage() else monster.get_hp()
        print(f"Attacked {monster.get_name()}, damage {damage} hp")
        monster.set_hp(monster.get_hp() - damage)

        if monster.get_hp() == 0:
            print(f"{monster.get_name()} died")
            desk.delete_character(x, y)
        else:
            print(f"{monster.get_name()} now has {monster.get_hp()} hp")
    else:
        print(f"No {name} here")
        return

class MUD(cmd.Cmd):
    prompt = '~~> '
    intro = "<<< Welcome to Python-MUD 0.1 >>>\nType help or ? to list commands.\n"

    #cmd settings
    def do_EOF(self, args):
        '''Ctr+D to quit game'''
        print('\n')
        return True 

    # move hero
    def do_up(self, args):
        '''Moves the hero up one square'''
        move_hero("up")
    def do_down(self, args):
        '''Moves the hero down one square'''
        move_hero("down")
    def do_left(self, args):
        '''Moves the hero left one square'''
        move_hero("left")
    def do_right(self, args):
        '''Moves the hero right one square'''
        move_hero("right")

    # attack monsters
    def do_attack(self, args):
        attack(args)
    def help_attack(self):
        print("attack <name_str> {with <weapon_name>} - Hero attacks monster with name == <name_str> on current square")
        print("\n{with <weapon_name>} - choose weapon (default 'sword': damage 10 hp)")
    def complete_attack(self, text, line, begidx, endidx):
        line = shlex.split(line)
        res = cowsay.list_cows() + list(custom_monsters.keys())

        if line[-1] == "with" or line[-2] == "with":
            return [i for i in hero.get_armory() if i.startswith(text)]
        elif line[-1] == "attack" or line[-2] == "attack":
            return [i for i in res if i.startswith(text)]

    # add new monster on field
    def do_addmon(self, args):
        addmon(args)
    def help_addmon(self):
        print("addmon <name> [hello <hello_string>] [hp <value>] [coords <x> <y>]")
        print("\n<name> - name of monster")
        print("[hello <hello_string>] - phrase that monster say on meeting with hero")
        print("[hp <value>] - monster hp (must be integer and above zero)")
        print("[coords <x> <y>] - coords square on field, where monster will stand (if there is already a monster on the square, then replaces it)")
        print("\t<x> <y> must be integer in [0, 9]")

if __name__=='__main__':
    desk = Field()
    hero = Hero(desk)

    MUD().cmdloop()