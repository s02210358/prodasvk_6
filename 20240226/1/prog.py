import sys
import cowsay

class Field:
	def __init__(self):
		self.char_pos = {}
	def get_character(self, x: int, y:int):
		return self.char_pos[(x, y)]
	def check_position(self, x: int, y: int):
		return (x, y) in self.char_pos.keys()
	def set_character(self, x: int, y: int, other):
		self.char_pos[(x, y)] = other
	def encounter(self, x, y):
		monster = self.get_character(x, y)
		print(cowsay.cowsay(monster.get_phrase()))

class Character:
	x = 0
	y = 0

	def __init__(self, field: Field):
		self.field = field

	def get_position(self):
		return (self.x, self.y)
	def set_position(self, x: int, y: int):
		self.x, self.y = x, y

class Monster(Character):
	def __init__(self, x: int, y: int, phrase: str, field: Field):
		super().__init__(field)
		self.x = x
		self.y = y
		self.phrase = phrase
		self.field.set_character(self.x, self.y, self)
	def get_phrase(self):
		return self.phrase

desk = Field()
hero = Character(desk)

while (line :=  sys.stdin.readline()):
	match line.split():
		case [('up'|'down'|'left'|'right') as move]:
			x, y = hero.get_position()

			match move:
				case 'up':
					y = (y - 1)%10
				case 'down':
					y = (y + 1)%10
				case 'left':
					x = (x - 1)%10
				case 'right':
					x = (x + 1)%10

			hero.set_position(x, y)
			print(f'Moved to ({x}, {y})')
			
			if desk.check_position(x, y):
				desk.encounter(x, y)
		case ['addmon', *opt]:
			try:
				x, y, phrase = int(opt[0]), int(opt[1]), opt[2]
			except Exception:
				print('Invalid arguments')

			flag = False

			if (len(opt) == 3) and (x < 10 and x >= 0)\
					and (y < 10 and y >= 0):
				if desk.check_position(x, y):
					flag = True

				Monster(x, y, phrase, desk)
				print(f'Added monster to ({x}, {y}) saying {phrase}')

				if flag: print('Replaced the old monster')
			else:
				print('Invalid arguments')
		case _:
			print('Invalid command')
