import cowsay
import sys

with open("batman.cow", "r") as f:
    cow = cowsay.read_dot_cow(f)

print(cowsay.cowsay(sys.argv[1], cowfile=cow))
