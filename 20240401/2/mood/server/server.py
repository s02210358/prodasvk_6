import io
import shlex
import asyncio
import cowsay
from ..common import custom_monsters


class Field:
    def __init__(self):
        self.char_pos = {}

    def get_character(self, x: int, y: int):
        return self.char_pos[(x, y)]

    def check_position(self, x: int, y: int):
        return (x, y) in self.char_pos.keys()

    def set_character(self, x: int, y: int, other):
        self.char_pos[(x, y)] = other

    def delete_character(self, x: int, y: int):
        del self.char_pos[(x, y)]

    def encounter(self, x, y):
        monster = self.get_character(x, y)
        name = monster.get_name()

        if name in cowsay.list_cows():
            return cowsay.cowsay(monster.get_phrase(), cow=name)
        elif name in custom_monsters:
            custom_cow = cowsay.read_dot_cow(io.StringIO(custom_monsters[name]))
            return cowsay.cowsay(monster.get_phrase(), cowfile=custom_cow)


class Character:
    x = 0
    y = 0

    def __init__(self, field: Field):
        self.field = field

    def get_position(self):
        return (self.x, self.y)

    def set_position(self, x: int, y: int):
        self.x, self.y = x, y


class Hero(Character):
    def __init__(self, field: Field):
        super().__init__(field)
        self.x = 0
        self.y = 0
        self.weapon = "sword"
        self.armory = {"sword": 10,
                       "spear": 15,
                       "axe": 20}

    def choose_weapon(self, name):
        if name in self.armory.keys():
            self.weapon = name
            return True
        return False

    def get_damage(self):
        return self.armory[self.weapon]

    def get_armory(self):
        return self.armory.keys()


class Monster(Character):
    def __init__(self, x: int, y: int, name: str, phrase: str, hp: int, field: Field):
        super().__init__(field)
        self.x = x
        self.y = y
        self.name = name
        self.phrase = phrase
        self.hp = hp
        self.field.set_character(self.x, self.y, self)

    def get_phrase(self):
        return self.phrase

    def get_name(self):
        return self.name

    def set_hp(self, new_hp: int):
        self.hp = new_hp

    def get_hp(self):
        return self.hp


clients = {}
desk = Field()


async def mud(reader, writer):
    log_in = asyncio.create_task(reader.readline())
    log_res = await log_in
    res_arr = log_res.decode().strip().split()
    me = ''

    match res_arr:
        case ["login", name]:
            if name in clients:
                writer.write(f'{0}\n'.encode())
                await writer.drain()
                reader.feed_eof()
                await reader.read()
            else:
                me = name
                writer.write(f'{1}\n'.encode())
                print(f"log in user: {me}")

    hero = Hero(desk)
    clients[me] = asyncio.Queue()
    send = asyncio.create_task(reader.readline())
    receive = asyncio.create_task(clients[me].get())

    if me:
        for el in clients.values():
            if el is not clients[me]:
                await el.put(f"User {me} connected")

    while not reader.at_eof():
        done, pending = await asyncio.wait([send, receive], return_when=asyncio.FIRST_COMPLETED)

        for q in done:
            if q is send:
                send = asyncio.create_task(reader.readline())
                text = q.result().decode().strip()

                match shlex.split(text):
                    case ["move", a, b]:
                        x, y = hero.get_position()
                        x = (x + int(a)) % 10
                        y = (y + int(b)) % 10
                        hero.set_position(x, y)

                        await clients[me].put(f"Moved to ({x}, {y})")

                        if desk.check_position(x, y):
                            msg = desk.encounter(x, y)
                            await clients[me].put(msg)
                    case ["addmon", name, "phrase", phrase, "hp", hp, "coords", x, y]:
                        x, y = int(x), int(y)
                        hp = int(hp)

                        flag = desk.check_position(x, y)

                        Monster(x, y, name, phrase, hp, desk)
                        await clients[me].put(f'Added monster {name} to ({x}, {y}) saying: "{phrase}"')

                        for el in clients.values():
                            if el is not clients[me]:
                                await el.put(f'User {me} added monster {name} with {hp} hp')

                        if flag:
                            await clients[me].put('Replaced the old monster')
                    case ["attack", name, weapon]:
                        x, y = hero.get_position()

                        if desk.check_position(x, y)\
                                and (monster := desk.get_character(x, y)).get_name() == name:
                            hero.choose_weapon(weapon)
                            damage = hero.get_damage() if monster.get_hp() >= hero.get_damage()\
                                else monster.get_hp()
                            monster.set_hp(monster.get_hp() - damage)
                            await clients[me].put(f"Attacked {monster.get_name()}, damage {damage} hp")

                            if monster.get_hp() == 0:
                                await clients[me].put(f"{monster.get_name()} died")
                                desk.delete_character(x, y)
                            else:
                                await clients[me].put(f"{monster.get_name()} now has {monster.get_hp()} hp")

                            for el in clients.values():
                                if el is not clients[me]:
                                    tmp1 = f"User {me} attacked monster {name} with {weapon}, damage {damage} hp"
                                    tmp2 = f"\n{name} now has {monster.get_hp()} hp"\
                                        if monster.get_hp() != 0 else f"\n{name} died"
                                    await el.put(tmp1 + tmp2)
                        else:
                            await clients[me].put(f"No {name} here")
                    case ["quit"]:
                        reader.feed_eof()
                        await reader.read()
                        break
                    case ["sayall", text]:
                        for el in clients.values():
                            if el is not clients[me]:
                                await el.put(f"{me}: {text}")
                    case _:
                        print(text)
            elif q is receive:
                receive = asyncio.create_task(clients[me].get())
                writer.write(f"{q.result()}\n".encode())
                await writer.drain()

    send.cancel()
    receive.cancel()

    if me:
        print(f"{me} disconnected")
        for el in clients.values():
            if el is not clients[me]:
                await el.put(f'{me} disconnected')

    del clients[me]
    writer.close()
    await writer.wait_closed()
