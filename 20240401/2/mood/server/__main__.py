from ..server import server as srv


async def main():
    server = await srv.asyncio.start_server(srv.mud, '0.0.0.0', 1337)
    async with server:
        await server.serve_forever()

srv.asyncio.run(main())
