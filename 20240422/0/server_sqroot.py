import asyncio
from sqroot import sqroots

async def echo(reader, writer):
    data = await reader.readline()
    try:
        writer.write((sqroots(data.decode()) + "\n").encode())
    except Exception:
        writer.write("\n".encode())
    writer.close()
    await writer.wait_closed()

async def main():
    server = await asyncio.start_server(echo, '0.0.0.0', 1337)
    async with server:
        await server.serve_forever()

def server():
    asyncio.run(main())
