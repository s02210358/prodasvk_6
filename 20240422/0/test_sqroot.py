import sqroot
import client_sqroot
import server_sqroot
form unittest.mock import MagicMock
import unittest

class TestSqroot(unittest.TestCase):

    def test_0(self):
        self.assertEqual(sqroot.sqroots("1 2 1"), "-1.0")

    def test_1(self):
        self.assertEqual(sqroot.sqroots("1 3 4"), "")
    
    def test_2(self):
        self.assertEqual(sqroot.sqroots("1 -5 4"), "4.0 1.0")

    def test_3(self):
        with self.assertRaises(Exception):
            sqroot.sqroots("1 2 asdasd")

class TestSqrootNet(unittest.TestCase):

    def SetUp(self):
        self.s = MagicMock()
        self.s.sendall = lambda par: setattr(self, "res", sqroots.sqroot(par))
        self.s.recv = lambda args: self.res

    def test_0(self):
        self.assertEqual(client_sqroot.sqrootnet("1 2 1", self.s), "-1.0")

    def test_1(self):
        self.assertEqual(client_sqroot.sqrootnet("1 3 4", self.s), "")
    
    def test_2(self):
        self.assertEqual(sqroot.sqroots("1 -5 4"), "4.0 1.0")

    def test_3(self):
        with self.assertRaises(Exception):
            sqroot.sqroots("1 2 asdasd")
  
