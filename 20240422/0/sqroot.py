import math

def sqroots(coeffs: str) -> str:
    a, b, c = map(float, coeffs.split())
    
    d = b**2 - 4 * a * c

    if d < 0:
        return ""

    x_1 = (-b + math.sqrt(d))/ (2 * a)
    x_2 = (-b - math.sqrt(d))/ (2 * a)

    if x_1 != x_2:
        return f"{x_1} {x_2}"
    else:
        return f"{x_1}"
if __name__ == "__main__":
    sqroots(input())
