"""Start server and roaming_monster function."""
from ..server import server as srv
import sys

port = 1337 if len(sys.argv) < 2 else int(sys.argv[1])


async def main():
    """Start roaming_monster and asyncio server."""
    server = await srv.asyncio.start_server(srv.mud, '0.0.0.0', port)
    func = srv.roaming_monster()
    task = srv.asyncio.create_task(func)
    await srv.asyncio.sleep(0)
    async with server:
        await server.serve_forever()

srv.asyncio.run(main())
