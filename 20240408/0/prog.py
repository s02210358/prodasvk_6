import readline
import cmd
import sys

class tmp(cmd.Cmd):
    def do_EOF(self, args):
        return True

    def do_bless(self, args):
        print(f"Be blessed, {args}!")
    def do_sendto(self, args):
        print(f"Go to {args}")
if __name__ == "__main__":
    if len(sys.argv) > 1:
        file_name = sys.argv[1]
        with open(file_name, "r") as f:
            saytmp = tmp(stdin=f)
            saytmp.prompt = ""
            saytmp.use_rawinput = False
            saytmp.cmdloop()
    else:
        saytmp = tmp()    
        saytmp.cmdloop()

