"""Dump prog."""
import calendar
import sys

if __name__ == "__main__":
    """Simple calendar."""
    calen = calendar.month(int(sys.argv[1]), int(sys.argv[2]))
    
    calen = (".. table:: " + calen.lstrip()).split("\n")
    res = calen[0]
    
    for i in range(1, len(calen)):
        if i == 1:
            res += "\n\n" + "    == == == == == == ==\n"
        if i == 2:
            res += "    == == == == == == ==\n"
        if i == len(calen) - 1:
            res += "    == == == == == == =="
    
        res += f"    {calen[i]}\n"
    print(res, end='')
