.. ilya documentation master file, created by
   sphinx-quickstart on Mon Apr  8 13:30:42 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ilya's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   calen
   new

.. literalinclude:: restcalend/__init__.py
   :linenos:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
